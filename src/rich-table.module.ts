import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RichTableService } from './rich-table.service';
import { RichTableComponent } from './rich-table.component';

export function seedServiceFactory() {
	return new RichTableService();
}

@NgModule({
	imports: [CommonModule],
	providers: [
		{ provide: RichTableService, useFactory: seedServiceFactory }
	],
	declarations: [RichTableComponent],
	exports: [RichTableComponent]
})
export class RichTableModule { }
