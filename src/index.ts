export { RichTableService } from './rich-table.service';
export { RichTableComponent } from './rich-table.component';
export { RichTableModule } from './rich-table.module';