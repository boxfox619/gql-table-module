export default {
	entry: 'dist/index.js',
	dest: 'dist/bundles/rich-table.umd.js',
	sourceMap: false,
	format: 'umd',
	moduleName: 'ng.rich-table'
}
